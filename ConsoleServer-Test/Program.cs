﻿using System.Text;
using System.Threading.Channels;
using Serilog;
using Serilog.Events;
using Spectre.Console;
using ConsoleServer;
using Spectre.Console.Advanced;

//Setup Serilog
Log.Logger = new LoggerConfiguration()
    .MinimumLevel.Debug() 
    .Enrich.FromLogContext()
    .WriteTo.ConsoleLocal()  // If you want LocalConsole output
    .WriteTo.ConsoleTelnetServer() // Add our Telnet server. 
    .CreateLogger();

var telnetServer = new ConsoleServer.TelnetServer(3333);

Action<TelnetSession> newSession = session =>
{
    var ctrlCPressed = false; 
    
    //Capture key presses.
    session.AnsiConsole.Input.KeyPressed += (sender, consoleKeyInfo) =>
    {
        if (consoleKeyInfo.Key == ConsoleKey.Escape)
        {
            Log.Information("Escape Pressed!");
        }
        else if (consoleKeyInfo.Key == ConsoleKey.C && consoleKeyInfo.Control)
        {
            ctrlCPressed = true;
        }
    };
    
    try
    {
        do
        {
            var logLevel = session.AnsiConsole.Prompt(
                new SelectionPrompt<LogEventLevel>()
                    .Title("Select [green]Log Level[/]")
                    .AddChoices(new[] { LogEventLevel.Verbose,LogEventLevel.Debug,LogEventLevel.Information,
                                        LogEventLevel.Warning, LogEventLevel.Error, LogEventLevel.Fatal}));
            
            session.LogLevel = logLevel;

            for (int i=0; i<10; i++)
            {
                if (ctrlCPressed) break;
                var rule = new Rule($"[red]Line {i}[/]");
                rule.Alignment = Justify.Left;
                session.AnsiConsole.Write(rule);
                Log.Information("Information {i}",i);
                Log.Verbose("Verbose {i}",i);
                Log.Error("Oops! {i}",i);
                Log.Debug("Debug {i}",i);
                Log.Fatal("Fatal {i}",i);
                Log.Warning("Warning {i}",i);
                Thread.Sleep(800);
            }
        } while (!ctrlCPressed);
        
        session.AnsiConsole.WriteLine();
        session.AnsiConsole.Write(new FigletText("Bye!")
                .LeftAligned()
                .Color(Color.Yellow));
    }
    catch (Exception e)
    {
        Log.Error(e,"Console");
    }
};

telnetServer.StartListen(newSession);

Console.ReadLine();