namespace ConsoleServer;

public enum ReasonForClosure
{
    ClientDissconnected,
    ServerClosing,
    SocketError
}

public enum TelnetCommands : byte
{
    IAC  = 255,     //Command
    SE   = 240,    //end of subnegotation parameters
    NOP  = 241,    // no-op
    DM   = 242,    // Data Mark portion of SYNCH
    BRK  = 243,    // NVT character BRK (indicating Break or Attention key was hit)
    IP   = 244,    // Interrupt Process
    AO   = 245,    // Abort Output
    AYT  = 246,    // Are You There
    EC   = 247,    // Erase Character
    EL   = 248,    // Erase Line
    GA   = 249,    // Go Ahead
    SB   = 250,    // subnegotation

// Negotation codes:

    WILL = 251,
    WONT = 252,
    DO   = 253,
    DONT = 254,
}

public enum TelnetOptions : byte
{
    TRANSMIT_BIN = 0,
    ECHO = 1,
    RECON = 2,
    SUPPRESS_GA = 3,
    APPROX_MSG_SIZE_NEG = 4,
    STATUS = 5,
    TIMING_MARK = 6,
    RCTE = 7,
    OUT_LINE_WIDTH = 8,
    OUT_PAGE_SIZE = 9,
    NAOCRD = 10,
    NAOHTS = 11,
    NAOHTD = 12,
    NAOFFD = 13,
    NAOVTS = 14,
    NAOVTD = 15,
    NAOLFD = 16,
    EXT_ASCII = 17,
    LOGOUT = 18,
    BM = 19,
    DET = 20,
    SUPDUP = 21,
    SUPDUP_OUT = 22,
    SEND_LOC = 23,
    TERMINAL_TYPE = 24,
    EOR = 25,
    TUID = 26,
    OUTMRK = 27,
    TTYLOC = 28,
    N3270_REGIME = 29,

    X3PAD = 30,
    NAWS = 31,
    TERMINAL_SPEED = 32,
    TOOGLE_FLOW_CTRL = 33,
    LINEMODE = 34,
    XDISPLOC = 35,
    ENVIRON = 36,
    AUTHN = 37,
    ENCRYPT = 38,
    NEW_ENVIRON = 39,
    TN3270E = 40,
    XAUTH = 41,
    CHARSET = 42,
    RSP = 43,
    COM_PORT = 44,
    SUPPRESS_LOCAL_ECHO = 45,
    START_TLS = 46,
    KERMIT = 47, // y'know Kermit the frog? ^^ Maybe derived from him...
    SEND_URL = 48,
    FORWARD_X = 49,

//Big gap with unassigned option codes...

    TELOPT_PRAGMA_LOGON = 138,
    TELOPT_SSPI_LOGON = 139,
    TELOPT_PRAGMA_HEARTBEAT = 140,

//Another big gap with unassigned option codes...

    EXOPL = 255
}