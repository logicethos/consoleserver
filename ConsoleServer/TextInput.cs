using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Spectre.Console;

namespace ConsoleServer;

    public sealed class TextInput : IAnsiConsoleInput
    {
        private readonly Queue<ConsoleKeyInfo?> _input;
        private ManualResetEvent keypressWait = new ManualResetEvent(false);
        public EventHandler<ConsoleKeyInfoExt> KeyPressed;

        public TextInput()
        {
            _input = new Queue<ConsoleKeyInfo?>();
        }

        public void PushCharacter(char input)
        {
            switch (input)
            {
                case (char)9:
                    PushKey(ConsoleKey.Tab);
                    break;
                case < (char)26: //Ctrl
                    PushKey(new ConsoleKeyInfoExt( (char)(64+input), 64+(ConsoleKey)input, false, false, true,new byte[]{(byte)input} ));
                    break;
                case '↑':
                    PushKey(ConsoleKey.UpArrow);
                    break;
                case '↓':
                    PushKey(ConsoleKey.DownArrow);
                    break;
                case '↲':
                    PushKey(ConsoleKey.Enter);
                    break;
                case '¦':
                    _input.Enqueue((null));
                    keypressWait.Set();
                    break;
                default:
                    var shift = char.IsUpper(input);
                    PushKey(new ConsoleKeyInfoExt(input, (ConsoleKey)input, shift, false, false,new byte[]{(byte)input}));
                    break;
            }
        }

        public void PushKey(byte[] telnetBytes, ConsoleKey input)
        {
            PushKey((new ConsoleKeyInfoExt((char)input, input, false, false, false,telnetBytes)));
        }

        public void PushKey(ConsoleKey input)
        {
            PushKey(new ConsoleKeyInfoExt((char)input, input, false, false, false));
        }

        public void PushKey(ConsoleKeyInfoExt input)
        {
            KeyPressed?.Invoke(this, input);
            if (!input.Cancel)
            {
                _input.Enqueue(input.ConsoleKeyInfo);
                keypressWait.Set();
            }
        }
        
        public bool IsKeyAvailable()
        {
            return _input.Count > 0;
        }

        public ConsoleKeyInfo? ReadKey(bool intercept)
        {
            if (_input.Count == 0)
            {
                keypressWait.Reset();
                keypressWait.WaitOne();
            }

            var result = _input.Dequeue();

            return result;
        }

        public void ClearInput()
        {
            _input.Clear();
        }
        
        public Task<ConsoleKeyInfo?> ReadKeyAsync(bool intercept, CancellationToken cancellationToken)
        {
            return Task.FromResult(ReadKey(intercept));
        }
    }
