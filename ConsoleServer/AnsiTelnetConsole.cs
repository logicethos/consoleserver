

using System;
using Serilog;
using Spectre.Console;
using Spectre.Console.Rendering;

namespace ConsoleServer;

    public sealed class AnsiTelnetConsole : IAnsiConsole
    {
        private readonly IAnsiConsole _console;
        private readonly TextInput _input;

        public Profile Profile => _console.Profile;

        public IAnsiConsoleCursor Cursor => _console.Cursor;

        IAnsiConsoleInput IAnsiConsole.Input => _input;

        public TextInput Input => _input;

        public IExclusivityMode ExclusivityMode => _console.ExclusivityMode;

        public RenderPipeline Pipeline => _console.Pipeline;

        public AnsiTelnetConsole(IAnsiConsole console)
        {
            _console = console ?? throw new ArgumentNullException(nameof(console));
            _input = new TextInput();
        }

        public TelnetSession TelnetSession { get; set; }
        
        public void Clear(bool home)
        {
            try
            {
                if (TelnetSession.Connected) _console.Clear(home);
            }
            catch (Exception e)
            {
                Log.Error(e,"Failed to clear");
            }
        }

        public void Write(IRenderable renderable)
        {
            try
            {
                if (TelnetSession.Connected) _console.Write(renderable);
            }
            catch (Exception e)
            {
                Log.Error(e,"Failed to write");
            }
        }
    }
