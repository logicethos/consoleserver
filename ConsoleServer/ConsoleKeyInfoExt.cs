namespace ConsoleServer;

public class ConsoleKeyInfoExt
{
    public char KeyChar;
    public ConsoleKey Key;
    public bool Shift;
    public bool Alt;
    public bool Control;
    public byte[]? TelnetBytes;

    public bool Cancel = false;

    public ConsoleKeyInfoExt(ConsoleKeyInfo consoleKeyInfo)
    {
        KeyChar = consoleKeyInfo.KeyChar;
        Key = consoleKeyInfo.Key;
        Shift = (consoleKeyInfo.Modifiers & ConsoleModifiers.Shift) != 0;
        Alt = (consoleKeyInfo.Modifiers & ConsoleModifiers.Alt) != 0;
        Control = (consoleKeyInfo.Modifiers & ConsoleModifiers.Control) != 0;
    }
    
    public ConsoleKeyInfoExt(char keyChar, ConsoleKey key, bool shift, bool alt, bool control, byte[] telnetBytes = null)
    {
        this.KeyChar = keyChar;
        this.Key = key;
        this.Shift = shift;
        this.Alt = alt;
        this.Control = control;
        this.TelnetBytes = telnetBytes;
    }

    public ConsoleKeyInfo ConsoleKeyInfo
    {
        get
        {
            return new ConsoleKeyInfo(KeyChar, Key, Shift, Alt, Control);
        }
    }
    
}