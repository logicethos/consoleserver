﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Sockets;
using System.Net;
using System.Threading;
using System.Threading.Channels;
using Serilog;
using Serilog.Core;
using Serilog.Events;
using Spectre.Console;

namespace ConsoleServer;

public class TelnetServer : IDisposable
{
    private ILogger _logger;

    //Socket Connections
    private static List<TelnetServer> TelnetServerList = new List<TelnetServer>();
    private List<TelnetSession> TelnetClientList = new List<TelnetSession>();

    //Listener Socket
    Socket socketListener;
    public uint MaximumConnections { get; init; }
    public IPEndPoint LocalEndPoint { get; init; }
    public bool DisableEcho  { get; set; } = true;

    private static Channel<(LogEvent, byte[])>? _logChannel = null;
    public static Channel<(LogEvent, byte[])> LogChannel
    {
        get
        {
            if (_logChannel == null)
            {
                _logChannel = Channel.CreateUnbounded<(LogEvent, byte[])>();
                SerilogReader();
            }

            return _logChannel;
        }
    }
    
    public event EventHandler<TelnetSession> NewTelnetSession;
    private Action<TelnetSession>? SessionStartAction { get; set; }

    public TelnetServer(int port, uint maximumConnections = 500)
    {
        _logger = Log.ForContext(typeof(TelnetServer));
        LocalEndPoint = new IPEndPoint(IPAddress.Loopback, port);
        MaximumConnections = maximumConnections;
        TelnetServerList.Add(this);
    }

    public TelnetServer(IPEndPoint endpoint, uint maximumConnections = 500)
    {
        _logger = Log.ForContext(typeof(TelnetServer));
        LocalEndPoint = endpoint;
        MaximumConnections = maximumConnections;
        
        TelnetServerList.Add(this);
    }
    
    
    /// <summary>
    /// Start listening to incoming connections
    /// </summary>
    public void StartListen(Action<TelnetSession>? startAction = null)
    {
        try
        {
            SessionStartAction = startAction;
            _logger.Information("Listening port:{port} MaxSocks:{max})", LocalEndPoint.Port, MaximumConnections);

            socketListener = new Socket(LocalEndPoint.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            
            socketListener.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, 1);
            socketListener.Bind(LocalEndPoint);
            socketListener.Listen(100);
            socketListener.BeginAccept(new AsyncCallback(OnClientConnect), null);
        }
        catch (SocketException ex)
        {
            _logger.Error(ex, "StartListen: Listener Error:");
        }
    }


    /// <summary>
    /// New client waiting to connect
    /// </summary>
    /// <param name="asyn"></param>
    private void OnClientConnect(IAsyncResult asyn)
    {
        TelnetSession? session = null;

        try
        {
            // Suspend accepting new sockets
            Socket socket = (Socket)socketListener.EndAccept(asyn);

            if (TelnetClientList.Count < MaximumConnections)
            {
                //Create new client instance
                session = new TelnetSession(_logger, socket, this);
                //Add to client list
                lock (TelnetClientList)
                {
                    TelnetClientList.Add(session);
                }

                _logger.Verbose("Connected to {0}", socket.RemoteEndPoint.ToString());
                
                socket.BeginReceive(session.socketRXBuff, 0,
                    session.socketRXBuff.Length,
                    SocketFlags.None,
                    new AsyncCallback(OnDataReceived),
                    session);

                //Start new session app
                if(SessionStartAction!=null) Task.Run(() =>
                {
                    session.AnsiConsole.Input.ClearInput();  //Reset input queue
                    SessionStartAction(session);
                    session?.Dispose();
                });
                    
                NewTelnetSession?.Invoke(this,session);
            }
            else
            {
                _logger.Information("Max Socket Exceeded {0}", MaximumConnections);
            }

        }
        catch (Exception ex)
        {
            _logger.Error(ex, "OnConnect Error: {msg}", ex.Message);
            session?.Dispose(ReasonForClosure.SocketError);
        }
        finally
        {
            // Restart Socket listener
            socketListener?.BeginAccept(new AsyncCallback(OnClientConnect), null);
        }
    }

    public IPEndPoint[]  ConnectionList()
    {
        return TelnetClientList.Select(x => x.ipEndPoint).ToArray();
    }


    //incoming data from client
    public void OnDataReceived(IAsyncResult asyn)
    {
        try
        {
            //Get client
            TelnetSession session = (TelnetSession)asyn.AsyncState;
            //Stop RXing data for now
            int iRx = session.ClientSocket.EndReceive(asyn);

            if (iRx > 0)
            {
                var pt1 = session.DataReceived(iRx);
                if (pt1 > 0)
                {
                    Array.Copy(session.socketRXBuff, pt1, session.socketRXBuff, 0, session.socketRXBuff.Length - pt1);
                    pt1 = session.socketRXBuff.Length - pt1;
                }

                //Wait for data
                try
                {
                    session.ClientSocket.BeginReceive(session.socketRXBuff, pt1, session.socketRXBuff.Length - pt1, SocketFlags.None, new AsyncCallback(OnDataReceived), session);
                }
                catch (Exception ex)
                {
                    _logger.Error(ex, "ClientConnect - error {msg}", ex.Message);
                    session.Dispose(ReasonForClosure.SocketError);
                }
            }
            else
            {
                session.Dispose(ReasonForClosure.ClientDissconnected);
            }
        }
        catch (ObjectDisposedException)
        {
            _logger.Information("Telnet Session Disconnected");
        }
        catch (Exception se)
        {
            _logger.Error(se,"OnDataReceived error");
            ((TelnetSession)asyn?.AsyncState)?.Dispose(ReasonForClosure.SocketError);
        }
    }

    
    //Client closed - remove from list
    public void RemoveClient(TelnetSession client)
    {
        try
        {
            if (TelnetClientList.Contains((TelnetSession)client))
            {
                _logger.Verbose("Removing client {client}",client.ToString());
                lock (TelnetClientList)
                {
                    TelnetClientList.Remove((TelnetSession)client);
                }
            }
        }
        catch (Exception ex)
        {
            _logger.Error(ex, "RemoveClient: Disconect Error");
        }
    }


    public void DropConnections()
    {
        TelnetSession[] protoClientList_Clone;
        lock (TelnetClientList)
        {
            protoClientList_Clone = TelnetClientList.ToArray();
        }

        foreach (TelnetSession client in protoClientList_Clone)
        {
            try
            {
                client.Dispose(ReasonForClosure.ServerClosing);
            }
            catch
            {
            }
        }
    }

    static void SerilogReader()
    {
        Task.Run(async () =>
        {
            await foreach (var item in LogChannel.Reader.ReadAllAsync())
            {
                foreach (var server in TelnetServerList)
                {
                    foreach (var telnetSession in server.TelnetClientList)
                    {
                        telnetSession.LoggerEmit(item.Item1,item.Item2);
                    }
                }
            }
        });
    }

    public IEnumerable<TelnetSession> GetSessions()
    {
        lock (TelnetClientList)
        {
            return TelnetClientList.ToArray();
        }
    }

    public void Dispose()
    {
        TelnetServerList.Remove(this);
        _logger.Information("Closing Service");
        try
        {
            socketListener?.Close();
            DropConnections();
        }
        catch (Exception ex)
        {
            _logger.Error(ex, "Close Error");
        }
    }
}