﻿using System;
using System.ComponentModel;
using System.Text;
using System.IO;
using System.Net.Sockets;
using System.Net;
using Serilog;
using Serilog.Events;
using Spectre.Console;

namespace ConsoleServer;

/// <summary>
/// An incoming connection from a socket listener.
/// </summary>
public class TelnetSession : IDisposable 
{
    private ILogger _logger;
 
    //Comms
    public Socket ClientSocket { get; init; }

    //Buffers
    private const int socketBuffLen = 1024;
    public byte[] socketRXBuff = new byte[socketBuffLen]; //Keyboard input, and control sequences
    private int bufferHeadroom = socketBuffLen - 5;

    private readonly byte[] _rxEndofLineChars = new byte[]{10,13};
    private byte _txEndofLineChar = 10;
    private readonly byte[] crlf = new byte[] { (byte)'\r', (byte)'\n' };

    public UInt64 BytesRX { get; private set; } = 0;
    public UInt64 BytesTX  { get; private set; } = 0;

    TelnetServer ServerCLass { get; set; }
   
    NetworkStream networkStream;
    StreamWriter streamReader;
    
    //Time
    public DateTime TimeConnected  { get; private set; }
    public DateTime TimeRXData  { get; private set; }

    public IPEndPoint ipEndPoint { get; init; }

    public AnsiTelnetConsole AnsiConsole { get; init; }
    public event EventHandler<ReasonForClosure> Closing;
    public event EventHandler<(int Width,int Height)> ScreenResize;

    public LogEventLevel? LogLevel;
    
    private readonly object _writeLock = new object();
    Boolean EchoOn = false;
    public bool DisableEcho { get; private set; } = true;
    
    /// <summary>
    /// New connection from server class
    /// </summary>
    /// <param name="clientSocket">Socket of connection attempt</param>
    /// <param name="serverCLass">Instance of SocketServer calling this method</param>
    public TelnetSession(ILogger logger, Socket clientSocket, TelnetServer serverCLass, uint RXBufferSize = 50)
    {
        _logger = logger.ForContext(typeof(TelnetSession));
        TimeConnected = DateTime.UtcNow;
        TimeRXData = DateTime.UtcNow;
        DisableEcho = serverCLass.DisableEcho;

        ClientSocket = clientSocket;
        ServerCLass = serverCLass;

        ipEndPoint = (IPEndPoint)ClientSocket.RemoteEndPoint;
        
        networkStream = (Environment.NewLine.Length == 1) ? new NetworkStreamCRLF(ClientSocket) : new NetworkStream(ClientSocket);
        streamReader = new StreamWriter(networkStream);
        var consoleOutput = new AnsiConsoleOutput(streamReader);

        AnsiConsole = new AnsiTelnetConsole(Spectre.Console.AnsiConsole.Create(new AnsiConsoleSettings
        {
            Ansi = AnsiSupport.Yes,
            ColorSystem = ColorSystemSupport.Detect,
            Out = consoleOutput,
            Interactive = InteractionSupport.Yes,
            ExclusivityMode = null,
            Enrichment = null,
            EnvironmentVariables = null,
        }));
        AnsiConsole.TelnetSession = this;
        //Setup telnet modes
        Send(new byte[]
        {
            (byte)TelnetCommands.IAC, (byte)TelnetCommands.WILL, (byte)TelnetOptions.ECHO,
            (byte)TelnetCommands.IAC, (byte)TelnetCommands.WILL, (byte)TelnetOptions.SUPPRESS_GA,
            (byte)TelnetCommands.IAC, (byte)TelnetCommands.WONT, (byte)TelnetOptions.LINEMODE,
        });
        Send(new byte[] { (byte)TelnetCommands.IAC, (byte)TelnetCommands.DO, (byte)TelnetOptions.NAWS });
    }

    /// <summary>
    /// Called when data arrives at the socket.
    /// </summary>
    /// <param name="recv">Number of bytes received</param>
    public int DataReceived(int recv)
    {
        TimeRXData = DateTime.UtcNow;
        BytesRX += (UInt64)recv;

        int pt1 = 0;
        do
        {
            if (recv == socketBuffLen && pt1 >= bufferHeadroom) return pt1;  //stop pressing, so

            if (socketRXBuff[pt1] == (byte)TelnetCommands.IAC) // Telnet Command Char (255)
            {
                if (socketRXBuff[++pt1] == (byte)TelnetCommands.DO) //Telnet command char (253)
                {
                    if (socketRXBuff[++pt1] == (byte)TelnetOptions.ECHO) //Echo on requested
                    {
                        _logger.Debug("Echo On requested");
                        EchoOn = true;
                        pt1++;
                    }
                    else //unsupported command.
                    {
                        _logger.Debug("Unsupported: {b}", BitConverter.ToString(socketRXBuff, pt1 - 2, 3));
                        pt1++;
                    }
                }
                else if (socketRXBuff[pt1] == (byte)TelnetCommands.SB)  //Telnet subnegotiation
                {
                    if (socketRXBuff[++pt1] == (byte)TelnetOptions.NAWS)
                    {
                        AnsiConsole.Profile.Width = (socketRXBuff[++pt1] << 8) | socketRXBuff[++pt1];
                        AnsiConsole.Profile.Height = (socketRXBuff[++pt1] << 8) | socketRXBuff[++pt1];
                        ScreenResize?.Invoke(this,(AnsiConsole.Profile.Width,AnsiConsole.Profile.Height));
                        _logger.Debug("Screen Size: (w x h) {w} x {h}", AnsiConsole.Profile.Width, AnsiConsole.Profile.Height);
                    }
                    else
                    {
                        _logger.Debug("Unsupported Telnet subnegotiation: {b}", BitConverter.ToString(socketRXBuff, pt1 - 2, 3));
                    }

                    while (++pt1 < recv)
                    {
                        if (socketRXBuff[pt1] == (byte)TelnetCommands.SE)
                        {
                            pt1++;
                            break;
                        }
                    }
                }
                else if (socketRXBuff[pt1] == (byte)TelnetCommands.SE) //End of subnegotiation parameters.
                {
                    pt1 += 2;
                }
                else if (socketRXBuff[pt1] == (byte)TelnetCommands.WILL) //Sender wants to do something.
                {
                    if (socketRXBuff[++pt1] == (byte)TelnetOptions.NAWS)
                    {
                        _logger.Debug("WILL: NAWS Sent");
                        Send(new byte[] { (byte)TelnetCommands.IAC, (byte)TelnetCommands.DO, (byte)TelnetOptions.NAWS });
                        pt1++;
                    }
                    else
                    {
                        _logger.Debug("Unsupported WILL: {b}", BitConverter.ToString(socketRXBuff, pt1, 2));
                        pt1++; //unsupported command.
                    }
                }
                else
                {
                    _logger.Debug("Unsupported: {b}", BitConverter.ToString(socketRXBuff, pt1 - 1, 3));
                    pt1 += 2; //unsupported command.
                }
            }
            else if (socketRXBuff[pt1] == 27) //First Char is an Escape char
            {
                if (recv == 1)
                {
                    AnsiConsole.Input.PushKey(ConsoleKey.Escape);
                    pt1++;
                }
                else if (socketRXBuff[++pt1] == 91) //Terminal escape code...
                {
                    var bytes = new byte[recv - (pt1-1)];
                    Array.Copy(socketRXBuff,pt1-1,bytes,0,bytes.Length);
                    switch (socketRXBuff[++pt1])
                    {
                     //   case 49://home
                        case 72://home
                            AnsiConsole.Input.PushKey(bytes,ConsoleKey.Home);
                            break;
                        case 50: //insert
                            AnsiConsole.Input.PushKey(bytes,ConsoleKey.Insert);
                            break;
                        case 51: //del
                            AnsiConsole.Input.PushKey(bytes,ConsoleKey.Delete);
                            break;
                    //    case 52: //end
                        case 70: //end
                            AnsiConsole.Input.PushKey(bytes,ConsoleKey.End);
                            break;
                        case 53://Page Up
                            AnsiConsole.Input.PushKey(bytes,ConsoleKey.PageUp);
                            break;
                        case 54: //Page down
                            AnsiConsole.Input.PushKey(bytes,ConsoleKey.PageDown);
                            break;
                        case 65: //Up Arrow - Repeat last command
                            AnsiConsole.Input.PushKey(bytes,ConsoleKey.UpArrow);
                            break;
                        case 66: //Down arrow
                            AnsiConsole.Input.PushKey(bytes,ConsoleKey.DownArrow);
                            break;
                        case 67: //right arrow
                            AnsiConsole.Input.PushKey(bytes,ConsoleKey.RightArrow);
                            break;
                        case 68: //left arrow
                            AnsiConsole.Input.PushKey(bytes,ConsoleKey.LeftArrow);
                            break;
                        default:
                            Log.Debug("Unknown Key: {bytes}",BitConverter.ToString(bytes));
                            break;
                    }
                    pt1 = recv;
                }
                else if (socketRXBuff[pt1] == 79) //F Key
                {
                    var bytes = new byte[recv - (pt1-1)];
                    Array.Copy(socketRXBuff,pt1-1,bytes,0,bytes.Length);
                    if (socketRXBuff[++pt1] == 53) //Ctrl key
                    {  
                        AnsiConsole.Input.PushKey(new ConsoleKeyInfoExt((char)0, ConsoleKey.F1+(80-socketRXBuff[++pt1]), false, false, true,bytes));
                    }
                    else
                    {
                        AnsiConsole.Input.PushKey(bytes,ConsoleKey.F1+(80-socketRXBuff[pt1]));
                    }
                    pt1 = recv;
                }
            }
            else if (socketRXBuff[pt1] == (byte)TelnetOptions.NAOLFD) //https://datatracker.ietf.org/doc/html/rfc658
            {
                pt1++;
            }
            else if (socketRXBuff[pt1] == 127 || socketRXBuff[pt1] == 8) //Backspace
            {
                pt1++;
                AnsiConsole.Input.PushKey(ConsoleKey.Backspace);
            }
            else if (_rxEndofLineChars.Contains(socketRXBuff[pt1])) //e.g CR or LF
            {
                if (!ServerCLass.DisableEcho && EchoOn) Send(new byte[] { _txEndofLineChar });
                pt1++;
                if (pt1 <= recv - 1 && (socketRXBuff[pt1] ==0 || _rxEndofLineChars.Contains(socketRXBuff[pt1]))) pt1++;
                AnsiConsole.Input.PushKey(ConsoleKey.Enter);
            }
            else
            {
                AnsiConsole.Input.PushCharacter((char)socketRXBuff[pt1]);
                if (!ServerCLass.DisableEcho && EchoOn)
                {
                    lock (_writeLock)
                    {
                        ClientSocket.Send(socketRXBuff, pt1, 1, SocketFlags.None);
                    } // echo back the chars

                    BytesTX++;
                }
                pt1++;
            }
        } while (pt1 < recv);

        return 0;
    }

    public void LoggerEmit(LogEvent logEvent, byte[] textBytes)
    {
        if (logEvent?.Level >= LogLevel)
        {
           SendFixCR(textBytes,0,textBytes.Length);
        }
    }

    public void Send(byte[] bytes)
    {
        Send(bytes, 0, (uint)bytes.Length);
    }
    
    public void Send(byte[] buffer, uint offset, uint count)
    {
        lock (_writeLock)
        {
            ClientSocket.Send(buffer, (int)offset, (int)count, SocketFlags.None);
            BytesTX += count;
        }
    }
    
    /// <summary>
    /// Send data.  Telnet likes /r/n line termination.  This makes sure /n gets an /r.  Dont use for control codes. 
    /// </summary>
    /// <param name="buffer"></param>
    /// <param name="offset"></param>
    /// <param name="count"></param>
    public void SendFixCR(byte[] buffer, int offset, int count)
    {
        int pt1 = offset;
        int pt2 = offset;
        int pt3 = offset+count-1;
        lock (_writeLock)
        {
            while (pt1 <= pt3)
            {
                pt2 = Array.IndexOf(buffer, (byte)'\n', pt1);
                if (pt2 <= pt3 && pt2 >= 0 && (pt2 == 0 || buffer[pt2 - 1] != (byte)'\r'))
                {
                    ClientSocket.Send(buffer, pt1, pt2 - pt1, SocketFlags.None);
                    ClientSocket.Send(crlf, 0, crlf.Length, SocketFlags.None);
                    pt1 = pt2 + 1;
                }
                else
                {
                    //If we got here, then we assume the rest of the data is fine. We are not here to fix problems upstream.
                    ClientSocket.Send(buffer, pt1, count - pt1, SocketFlags.None);
                    return;
                }
            }
        }
    }
    

    
    public void sendText(String sMessage)
    {
        try
        {
            byte[] bSendBytes = Encoding.ASCII.GetBytes(sMessage);
            Send(bSendBytes);
            BytesTX += (UInt64)bSendBytes.Length;
        }
        catch (Exception ex)
        {
            _logger.Error(ex, "Error - unable to send message");
            Dispose(ReasonForClosure.SocketError);
        }
    }

    public IPEndPoint[]  ConnectionList()
    {
        return ServerCLass.ConnectionList();
    }

    public bool Connected
    {
        get { return ClientSocket?.Connected ?? false; }
    }

    public void Dispose()
    {
        Dispose(ReasonForClosure.ServerClosing);
    }
    
    public void Dispose(ReasonForClosure reason)
    {
        try
        {
            Closing?.Invoke(this, reason);
        }
        catch
        {
        }

        try
        {
            if (ClientSocket != null)
            {
                ClientSocket.Shutdown(SocketShutdown.Both);
                ClientSocket.Close();
            }
        }
        catch
        {
        }
        ServerCLass?.RemoveClient(this);
        ClientSocket?.Dispose();
        networkStream?.Dispose();
        streamReader?.Dispose();
    }
}