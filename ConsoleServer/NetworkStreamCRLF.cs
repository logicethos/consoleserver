using System.Net.Sockets;
using System.Text;

namespace ConsoleServer;

public class NetworkStreamCRLF : NetworkStream
{
    readonly byte[] crlf = new byte[] { (byte)'\r', (byte)'\n' };
   
   
    /// <summary>
    /// Intercepts Stream Write, and replaces LF with CRLF
    /// </summary>
    /// <param name="socket"></param>
    public NetworkStreamCRLF(Socket socket) : base(socket)
    {
    }

    public override void Write(byte[] buffer, int offset, int count)
    {
        int pt1 = offset;
        int pt2 = offset;
        int pt3 = offset+count-1;

       while (pt1 <= pt3)
        {
            pt2 = Array.IndexOf(buffer, (byte)'\n',pt1);
            if (pt2 <= pt3 &&  pt2 >= 0 && (pt2 == 0 || buffer[pt2-1]!=(byte)'\r'))
            {
                base.Write(buffer, pt1, pt2 - pt1);
                base.Write(crlf, 0, crlf.Length);
                pt1 = pt2 + 1;
            }
            else
            {
                //If we got here, then we assume the rest of the data is fine. We are not here to fix problems upstream.
                base.Write(buffer, pt1, pt3-pt1+1);
                return;
            }
        }
    }
}