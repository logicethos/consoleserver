# ConsoleServer
### A Telnet Console Server with [Serilog](https://serilog.net) & [Spectre.Console](https://spectreconsole.net).
![ConsoleServer NuGet Version](https://img.shields.io/nuget/v/ConsoleServer.svg?style=flat&label=NuGet%3A%20ConsoleServer)
#### Suggested uses.
  - Real-time remote Logging.
  - Remote status & debugging console.
  - Build multiuser console apps.
  - Multiplayer text based games.
  - Remote UI for IoT devices.
  

#### How to use it.

Initialise Serilog
```
Log.Logger = new LoggerConfiguration()
.MinimumLevel.Debug()
.Enrich.FromLogContext()
.WriteTo.ConsoleTelnetServer()   //<--  add this to your Serilog
.CreateLogger();
```
Start Telnet Server. Listening to local connections on port 3333:
```
var telnetServer = new ConsoleServer.TelnetServer(3333);
telnetServer.StartListen( new Action<TelnetSession>((session) => 
{
     session.AnsiConsole.Markup("[underline red]Hello[/] World!");
     session.LogLevel = LogEventLevel.Debug;  //switch on logging
}));
```

Then connect using a telnet client:
```telnet localhost 3333```

Each connection runs in it's own task thread.  For more complex apps, pass the 'session' into a new class. You can switch the log output level independently for each session, or disable it (set to null).

If you want to capture key presses:
```
session.AnsiConsole.Input.KeyPressed += (sender, consoleKeyInfo) =>
{
// do stuff...
}
```

#### Security

Unless you specify otherwise, connections are only accepted from Localhost. Beyond that you need to create your own security.
Suggestions:

- ssh into your server first, and then do ```telnet localhost 3333```
- Use a reverse proxy, like NGINX or integrate [YARP](https://github.com/microsoft/reverse-proxy)
- hack .bashrc or ssh config files to redirect logins.

### Future ideas

  - Websocket support.
  - Create a web front end using https://pub.dev/packages/xterm
  - Built in [YARP](https://github.com/microsoft/reverse-proxy) or SSH wrapper.
  - Add support for other logging libraries.
